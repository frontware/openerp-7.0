#OpenERP 7.0#

##Get it from Git##
---------------

Git Command-Line Syntax 
    
If you do not have a local Git repository, then you should clone this repository, commit some files, and then push your commits back to Gitblit.
	
    > git clone https://git.frontware.co.th:8443/git/OpenERP/OpenERP7.git

If you get an error about https/ssl. You have to run this command once only:

    > git config --global http.sslVerify false 
	
If you already have a local Git repository with commits, then you may add this repository as a remote and push to it.
	
    > git remote add origin https://git.frontware.co.th:8443/git/OpenERP/OpenERP7.git
    > git push origin master 


##URLs##
----------


- [Alfresco](http://goo.gl/XPXUe)
- [Git](https://git.frontware.co.th:8443/git/OpenERP/OpenERP7.git)
- [Home](http://www.openerp.asia)
- [Official](http://www.openerp.com)
- [Nightly build from Launchpad](http://nightly.openerp.com/7.0/nightly/src/openerp-7.0-latest.tar.gz) 


##Scripts##

The original repository is on launchpad using bazaar VCS

To download bzr repositories:

-	bzr branch lp:openobject-server/7.0 server
-	bzr branch lp:openerp-web/7.0 server/web
-	bzr branch lp:openobject-addons/7.0 server/addons



2 scripts allow you to update from bazaar to git repository

 >./update.sh

This will load latest nightly build and add it to reposiotiry, a new tag with build number is added.





