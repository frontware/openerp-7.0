# Clone bazar
# Requieres git, bzr and  bzr-fastimport 

git init
bzr branch lp:openobject-server/7.0 server
bzr branch lp:openerp-web/7.0 server/web
bzr branch lp:openobject-addons/7.0 server/addons
git add server
git commit -a -m "1st commit, add server"
cd server
bzr fast-export --plain . | git fast-import     # Do the actual conversion
cd addons
bzr fast-export --plain . | git fast-import
cd ../web
bzr fast-export --plain . | git fast-import
cd ..
git commit -f master                                # Will reply 'Already on master'
rm -rf .bzr/   
git commit -m "Import from bzr"
git push
